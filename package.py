name = "natron"
version = "1.2.0"
authors = ["inria"]
description = "Natron is an open-source, crossplatform nodal, compositing software"
tools = ["natron", "NatronRenderer"]
# requires = ["python"]
# help = "file://{root}/help.html"
uuid = "017a77ab-76e9-4a14-8fb6-b3052103f7b2"
def commands():
    # env.PYTHONPATH.append("{root}/python")
    env.PATH.append("$BD_SOFTS/natron/{version}")
